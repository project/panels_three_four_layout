
This layout provide two grids of 3x3 and 4x4 slots that automatically adapt to the way panes are layed out. In practice it means that slots that are not used are removed from the display and panes fill in empty space automatically (i.e. they expand or shrink to fit).

INSTALLATION
Enable the module :)